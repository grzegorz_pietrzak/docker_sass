#!/bin/sh

apk add --update ruby ruby-dev libffi-dev build-base 
gem install sass --no-ri --no-rdoc
apk del build-base ruby-dev
rm -rf /var/cache/apk/*
